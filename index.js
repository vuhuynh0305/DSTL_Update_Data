var fs = require("fs");
var obj;
var timestamp = 1518742800; // Fri Feb 16 2018 08:00:00
var file;
var dirname = "data/";

function readFile(path) {
  return new Promise(function(resolve, reject) {
    fs.readFile(path, "utf8", function(err, data) {
      if (err) throw err;
      resolve(JSON.parse(data));
    });
  });
}

async function parseData(path) {
  obj = await readFile(path);
}

function processData() {
  // generate new name
  var fileNamePart = file.split("_");
  fileNamePart[1] = timestamp;
  newName = fileNamePart.join("_");
  // generate new url
  var urlPart = obj.video_metadata.url.split("_");
  var extension = urlPart[1].split(".")[1];
  urlPart[1] = timestamp + "." + extension;
  obj.video_metadata.url = urlPart.join("_");
  // update timestamp
  obj.video_metadata.time = timestamp;
  var apnresult = obj.apnresult;
  for (var i = 0; i < apnresult.length; i++) {
    apnresult[i].time = timestamp;
    timestamp++;
  }
}

function writeFile() {
  fs.writeFile("result/" + newName, JSON.stringify(obj, null, 4), "utf-8");
}

function run() {
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
      throw err;
      return;
    }
    filenames.forEach(function(filename) {
      file = filename;
      parseData(dirname + filename)
        .then(processData)
        .then(writeFile);
    });
  });
}

run();
